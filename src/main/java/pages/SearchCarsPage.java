package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import config.SeleniumDriver;
import locators.SearchCarsPageLocators;

public class SearchCarsPage {
	SearchCarsPageLocators searchCarsPageLocators = null;

	final static Logger logger = Logger.getLogger(SearchCarsPage.class);

	public SearchCarsPage() {
		this.searchCarsPageLocators = new SearchCarsPageLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), searchCarsPageLocators);
	}

	public void selectCarMaker(String carBrand) {
		try {
			Select selectCarMaker = new Select(searchCarsPageLocators.carMakerDropDown);
			selectCarMaker.selectByVisibleText("BMW");
			logger.info("Selected Car Maker: "+carBrand);
		} catch (Exception e) {
			logger.error("Failed to select car Maker "+e);
		}

	}

	public void selectCarModel(String carModel) {
		try{
		SeleniumDriver.getDriver().findElement(By.xpath(".//*[@id='block-system-main']/div/div/div/div/div/form"))
				.click();
		Select selectCarModel = new Select(searchCarsPageLocators.selectModelDropDown);
		selectCarModel.selectByVisibleText(carModel);
		logger.info("Selected Car Model: "+carModel);
		}catch(Exception e){
			logger.error("Failed to select Car Model "+e);
		}
	}

	public void selectLocation(String location) {
		try{
		SeleniumDriver.getDriver().findElement(By.xpath(".//*[@id='block-system-main']/div/div/div/div/div/form"))
				.click();
		Select selectLocation = new Select(searchCarsPageLocators.selectLocation);
		selectLocation.selectByVisibleText(location);
		logger.info("Selected Location: "+location);
		}catch(Exception e){
			logger.error("Failed to select location "+e);
		}
	}

	public void selectPrice(String price) {
		try{
		Select selectPrice = new Select(searchCarsPageLocators.priceList);
		selectPrice.selectByVisibleText(price);
		logger.info("Selected Price: "+price);
		}catch(Exception e){
			logger.error("Failed to select Price "+e);
		}
	}

	public void clickOnFindMyNextCarButton() {
		try{
		searchCarsPageLocators.findMyNextCarButton.click();
		logger.info("Clicked on Find My Next Button");
		}catch(Exception e){
			logger.error("Failed to click on 'MyNextCar' Button "+e);
		}
	}
}
