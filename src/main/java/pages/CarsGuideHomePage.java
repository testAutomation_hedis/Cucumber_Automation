package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import config.SeleniumDriver;
import locators.CarsGuideHomePageLocators;

public class CarsGuideHomePage {
	private CarsGuideHomePageLocators carsGuideHomePageLocators = null;
	private static Logger logger = Logger.getLogger(CarsGuideHomePage.class);

	public CarsGuideHomePage() {
		carsGuideHomePageLocators = new CarsGuideHomePageLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), carsGuideHomePageLocators);
	}

	public void click_on_CarsForSale() {
		try {
			Actions act = new Actions(SeleniumDriver.getDriver());
			act.moveToElement(carsGuideHomePageLocators.carsForSale_Menu).perform();
			logger.info("Clicked on CarsForSale Menu");
		} catch (Exception e) {
			logger.error("Failed to click on Cars For Sale Menu " + e);
		}
	}

	public void click_on_SearchCars() {
		try {
			carsGuideHomePageLocators.SearchCars.click();
			logger.info("Clicked on Search Cars");
		} catch (Exception e) {
			logger.error("Failed to click on Search Cars " + e);
		}
	}
}
