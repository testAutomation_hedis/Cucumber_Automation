package runner;

import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;


import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="src/test/resources/features",
				 glue = { "steps" },
				 monochrome=true,
				 plugin = {"com.cucumber.listener.ExtentCucumberFormatter:" })
public class FunctionalTest extends AbstractTestNGCucumberTests {
	@BeforeClass
	public static void setup() {
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		extentProperties.setReportPath("output/report.html");
	}

	@AfterClass
	public static void teardown() {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\extent-config.xml"));
		Reporter.setSystemInfo("user", System.getProperty("user.name"));
		Reporter.setSystemInfo("os", System.getProperty("os.name"));
		Reporter.setTestRunnerOutput("Sample test runner output message");
	}
}
