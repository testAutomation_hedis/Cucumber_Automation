package steps;

import org.testng.Assert;

import config.SeleniumDriver;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.CarsGuideHomePage;
import pages.SearchCarsPage;

public class SearchCarSteps {
	CarsGuideHomePage carsGuideHomePage= new CarsGuideHomePage();
	SearchCarsPage searchCarsPage = new SearchCarsPage();;
	@Given("^I am on the Home Page \"([^\"]*)\" of CarsGuide Website$")
	public void i_am_on_the_Home_Page_of_CarsGuide_Website(String url) throws Throwable {
	    SeleniumDriver.openPage(url);
	}

	@When("^I move to Car For Sale Menu$")
	public void i_move_to_Car_For_Sale_Menu(DataTable arg1) throws Throwable {
		carsGuideHomePage.click_on_CarsForSale();
	    
	}

	@When("^click on \"([^\"]*)\" link$")
	public void click_on_link(String arg1) throws Throwable {
		carsGuideHomePage.click_on_SearchCars();
	    
	}

	@When("^select carbrand as \"([^\"]*)\" from AnyMake dropdown$")
	public void select_carbrand_as_from_AnyMake_dropdown(String carBrand) throws Throwable {
		SeleniumDriver.waitForPageToLoad();
		searchCarsPage.selectCarMaker(carBrand);
		System.out.println("Car Brand selected");

	}

	@When("^select \"([^\"]*)\" as car model$")
	public void select_as_car_model(String carModel) throws Throwable {
		SeleniumDriver.waitForPageToLoad();
		searchCarsPage.selectCarModel(carModel);

	}

	@When("^select location as \"([^\"]*)\" from SelectLocation dropdown$")
	public void select_location_as_from_SelectLocation_dropdown(String location) throws Throwable {
		SeleniumDriver.waitForPageToLoad();
		searchCarsPage.selectLocation(location);
	}

	@When("^select \"([^\"]*)\" as price$")
	public void select_as_price(String price) throws Throwable {
		SeleniumDriver.waitForPageToLoad();
		searchCarsPage.selectPrice(price);

	}

	@When("^click on Find_My_Next_Car button$")
	public void click_on_Find_My_Next_Car_button() throws Throwable {
		SeleniumDriver.waitForPageToLoad();
		searchCarsPage.clickOnFindMyNextCarButton();
	}

	@Then("^I should see list of searched cars$")
	public void i_should_see_list_of_searched_cars() throws Throwable {
		System.out.println("Car List Found");

	}

	@Then("^the page title should be \"([^\"]*)\"$")
	public void the_page_title_should_be(String expectedPageTitle) throws Throwable {
		String ActualPageTitle = SeleniumDriver.getDriver().getTitle();
		System.out.println("Actual page title-->" + ActualPageTitle);
		System.out.println("Expected page title-->" + expectedPageTitle);
		Assert.assertEquals(expectedPageTitle, ActualPageTitle);

	}
}
