package steps;

import config.SeleniumDriver;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class DriverConfig {

	@Before
	public void setup(){
		SeleniumDriver.setUpDriver();
	}
	
	@After
	public void endSession(){
		SeleniumDriver.tearDown();
	}
}
